<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US">

<head>
    <meta charset="utf-8" />
    <title></title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="VeryCreative">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link rel="stylesheet" type="text/css" href="assets/css/iealert/style.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/fontawesome-all.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/jquery.mmenu.all.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/slick-theme.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <link rel="stylesheet" type="text/css" href="style.css?v=<?php echo time(); ?>" />

    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Encode+Sans:300,400,500,600,700&display=swap" rel="stylesheet">
    <!-- /fonts -->

</head>

<body class="preload">
    <div id="page">
        <header>
            <nav class="logo">
                <img src="" alt="">
            </nav>
            <nav class="nav-right">
            </nav>
        </header>

        <div class="header-fixed"></div>